"""
/***************************************************************************
Soil parameters selection 1.0
        begin                : 2018-09-24
        copyright            : (C) 2018 by Giacomo Titti and Giulia Bossi, CNR-IRPI
        email                : giacomo.titti@irpi.cnr.it
 ***************************************************************************/

/***************************************************************************
    Soil parameters selection 1.0                                          
    Copyright (C) 2018 by Giacomo Titti and Giulia Bossi CNR-IRPI            

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ***************************************************************************/
"""
#coding=utf-8
import sys
from main import GridConstruction
from main import TerrainFinder
test = TerrainFinder()
#folder of reference
test.pathRoot='/tmp'#head input folder
test.pathHome='/home/irpi'#work folder
############################sorting2steps
planeAlfa=1
ratio=5
test.variables['v1step']=planeAlfa
test.variables['v2step']=ratio
test.v2step=10#top n solutions
##############################
test.pathcsvfile1step= '/solution1step.txt'
test.pathcsvfile2step= '/solution2step.txt'
pathcsvfilePxxx= '/solutionPxxx.txt'
test.pathcsvfileP=pathcsvfilePxxx.replace('xxx', str(test.Nlist))
##############################
test.condition= 4#4 for double step quartile
test.GPS_inclinometers()
print('end')
